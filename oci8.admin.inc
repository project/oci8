<?php

/**
 * Creates the form used for connection settings.
 */
function oci8_admin_connection_form(&$form_state) {
  $form = array();
  $form['oci8']['connection'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database connection settings'),
    '#description' => t('These settings are needed to connect to the Oracle database.'),
    '#weight' => 0,
  );
  $form['oci8']['connection']['oci8_connection_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('oci8_connection_username', ''),
    '#description' => t('Enter the username for the Oracle database.'),
    '#required' => TRUE,
    '#weight' => -10,
  );
  $form['oci8']['connection']['oci8_connection_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('oci8_connection_password', ''),
    '#description' => t('Enter the password that accompanies the username.'),
    '#required' => TRUE,
    '#weight' => -9,
  );
  $form['oci8']['connection']['oci8_connection_db'] = array(
    '#type' => 'textfield',
    '#title' => t('Database instance'),
    '#default_value' => variable_get('oci8_connection_db', ''),
    '#description' => t('Enter the name of the Oracle database instance. See the !connection_string section in the PHP documentation for more information.', array('!connection_string' => '<em>' . l('connection_string', 'http://www.php.net/manual/en/function.oci-connect.php#function.oci-connect.parameters') . '</em>')),
    '#required' => TRUE,
    '#weight' => -8,
  );
  $form['oci8']['connection']['oci8_connection_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 0,
    '#access' => user_access('administer oci8'),
    '#submit' => array('oci8_admin_connection_form_submit'),
  );
  $status = oci8_admin_connection_status();
  $form['oci8']['connection']['oci8_connection_status'] = array(
    '#type' => 'item',
    '#title' => t('Connection status'),
    '#value' => '<span style="color:' . $status['color'] . '">' . $status['message'] . '</span>',
    '#weight' => 10,
  );
  return $form;
}

/**
 * Saves the connection settings.
 */
function oci8_admin_connection_form_submit($form, &$form_state) {
  variable_set('oci8_connection_username', $form_state['values']['oci8_connection_username']);
  variable_set('oci8_connection_password', $form_state['values']['oci8_connection_password']);
  variable_set('oci8_connection_db', $form_state['values']['oci8_connection_db']);
  drupal_set_message(t('Oracle connection settings saved successfully.'));
}

/**
 * Tests whether the connection works and outputs the error if not.
 */
function oci8_admin_connection_status() {
  @oci8_connect();
  $status = oci_error();
  if (!empty($status['message'])) {
    $status['color'] = 'red';
  }
  else {
    $status['color'] = 'green';
    $status['message'] = t('Connection OK');
  }
  return $status;
}

/**
 * Creates the form used for connection settings.
 */
function oci8_admin_node_form(&$form_state) {
  $form = array();
  $form['oci8']['node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node linking configuration'),
    '#description' => t('These settings are for linking a node to Oracle data.'),
    '#weight' => 0,
  );
  $type_list = node_get_types();
  $types = array();
  foreach($type_list as $type) {
    $types[$type->type] = $type->name;
  }
  $form['oci8']['node']['oci8_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Node types'),
    '#description' => t('Select the node type that Oracle should link a table to.'),
    '#options' => $types,
    '#default_value' => variable_get('oci8_node_type', ''),
    '#required' => TRUE,
    '#weight' => 1,
  );
  $query = oci8_query("SELECT TABLE_NAME FROM USER_TABLES");
  $tables = array('' => '');
  while ($table = oci8_fetch_array($query)) {
    $tables[$table['TABLE_NAME']] = $table['TABLE_NAME'];
  }
  if (count($tables) > 0) {
    $form['oci8']['node']['oci8_node_table'] = array(
      '#type' => 'select',
      '#title' => t('Oracle tables'),
      '#description' => t('Select the Oracle tables that will link to a node.'),
      '#options' => $tables,
      '#default_value' => variable_get('oci8_node_table', ''),
      '#required' => TRUE,
      '#weight' => 2,
    );
    if (variable_get('oci8_node_table', '') != '') {
      $query = oci8_query("SELECT * FROM ALL_TAB_COLUMNS WHERE TABLE_NAME='" . variable_get('oci8_node_table', '') . "'");
      $fields = array('' => '');
      while ($field = oci8_fetch_array($query)) {
        $fields[$field['COLUMN_NAME']] = $field['COLUMN_NAME'];
      }
      if (count($fields) > 0) {
        $form['oci8']['node']['oci8_node_field'] = array(
          '#type' => 'select',
          '#title' => t('Oracle fields for ' . variable_get('oci8_node_table', '')),
          '#description' => t('Select the field that will be used as the foreign key to the node ID.'),
          '#options' => $fields,
          '#default_value' => variable_get('oci8_node_field', ''),
          '#weight' => 3,
        );
      }
    }
  }
  $form['oci8']['node']['oci8_node_view'] = array(
    '#type' => 'radios',
    '#title' => t('View data'),
    '#description' => t('Show data while viewing the node'),
    '#options' => array(1 => t('Yes'), 0 => t('No')),
    '#default_value' => variable_get('oci8_node_view', 0),
    '#weight' => 4,
  );
  $form['oci8']['node']['oci8_node_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 10,
    '#access' => user_access('administer oci8'),
    '#submit' => array('oci8_admin_node_form_submit'),
  );
  // Hide the field list when changing tables
  drupal_add_js('$(document).ready(function(){
    $("#edit-oci8-node-table").change(function() {
      $("#edit-oci8-node-field").val("");
      $("#edit-oci8-node-field").parent().hide();
    });
  });', 'inline');
  return $form;
}

/**
 * Saves the node linking configuration.
 */
function oci8_admin_node_form_submit($form, &$form_state) {
  variable_set('oci8_node_type', $form_state['values']['oci8_node_type']);
  variable_set('oci8_node_table', $form_state['values']['oci8_node_table']);
  if (variable_get('oci8_node_table', '') != '') {
    variable_set('oci8_node_field', $form_state['values']['oci8_node_field']);
  }
  else {
    variable_del('oci8_node_field');
  }
  variable_set('oci8_node_view', $form_state['values']['oci8_node_view']);
  drupal_set_message(t('Oracle node linking configuration saved successfully.'));
}
