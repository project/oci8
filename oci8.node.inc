<?php

/**
 * Implementation of hook_nodeapi().
 */
function oci8_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  // Do nothing if it's not the correct node type
  if (variable_get('oci8_node_type', '') != $node->type || arg(0) == 'admin') {
    return;
  }
  // Do nothing if the node linking is not configured properly
  if (variable_get('oci8_node_table', '') == '' || variable_get('oci8_node_field', '') == '') {
    return;
  }
  switch ($op) {
    case 'alter':
      // Do nothing if not set to show
      if (variable_get('oci8_node_view', 0) == 0 || !user_access('view oracle data')) {
        break;
      }
      $data = array(
        '#type' => 'fieldset',
        '#title' => t('Oracle data'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#weight' => 10,
      );
      // Create static form elements
      foreach ($node->oci8 as $field => $value) {
        if (!is_null($value)) {
          $data[$field] = array(
            '#type' => 'item',
            '#title' => $field,
            '#value' => $value,
          );
        }
      }
      // Merge the rendered elements into the node body.
      $node->body .= drupal_render($data);
      break;
    case 'insert':
      break;
    case 'load':
      $data = array();
      $table = variable_get('oci8_node_table', '');
      $fk = variable_get('oci8_node_field', '');
      $query = oci8_query("SELECT * FROM $table WHERE $fk='" . $node->nid . "'");
      $data = oci8_fetch_array($query);
      $result = array();
      foreach ($data as $field => $value) {
        $result['oci8'][$field] = $value;
      }
      if (is_array($result)) {
        return $result;
      }
    case 'prepare':
      break;
    case 'update':
      break;
    case 'view':
      break;
  }
}
